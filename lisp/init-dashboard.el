;;; init-dashboard.el --- Completion with dashboard -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))



(provide 'init-dashboard)
;;; init-dashboard.el ends here
