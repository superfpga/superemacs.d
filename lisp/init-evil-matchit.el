;;; init-evil-matchit.el --- Completion with evil-matchit -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package evil-matchit
  :ensure t
  :init (global-evil-matchit-mode t)
  (setq evilmi-shortcut "m"))



(provide 'init-evil-matchit)
;;; init-evil-matchit.el ends here
