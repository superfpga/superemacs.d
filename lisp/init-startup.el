;;; init-startup.el --- Completion with startup -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


;;key for bashrc file
(defun open-bashrc-file()
  (interactive)
  (find-file "~/.bashrc"))


;;key for initialize file
(defun open-init-file()
  (interactive)
  (find-file "~/.emacs.d/init.el"))


;;key for leader file
(defun open-leader-file()
  (interactive)
  (find-file "~/.emacs.d/lisp/init-general.el"))


;;custom define
(defun open-local-file()
  (interactive)
  (find-file "~/.emacs.d/lisp/init-local.el"))


(global-set-key(kbd "<f1>") 'open-init-file)
(global-set-key(kbd "<f2>") 'open-leader-file)
(global-set-key(kbd "<f3>") 'open-local-file)
(global-set-key(kbd "<f4>") 'open-bashrc-file)



;; TransParent
;; (set-frame-parameter nil 'alpha 0.90)



(eval-after-load "artist"
  '(define-key artist-mode-map [(down-mouse-3)] 'artist-mouse-choose-operation)
  )


;; (setq org-agenda-files (list "~/.emacs.d/superemacs.org"))




;; Start maximised (cross-platf)
;; (add-hook 'window-setup-hook 'toggle-frame-maximized t)





(provide 'init-startup)
;;; init-startup.el ends here
