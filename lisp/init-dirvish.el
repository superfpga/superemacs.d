;;; init-dirvish.el --- Completion with dirvish -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package dirvish
  :ensure t)



(provide 'init-dirvish)
;;; init-dirvish.el ends here
