;;; init-yasnippet.el --- Completion with yasnippet -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(require-package 'yasnippet)
(yas-global-mode 1)


(require-package 'auto-yasnippet)
(require-package 'ivy-yasnippet)
(require-package 'common-lisp-snippets)




(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
