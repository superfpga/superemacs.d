;;; init-auto-save.el --- Completion with auto-save -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(auto-save-mode t)

(auto-save-visited-mode t)

;; (auto-save-visited-internal)



(provide 'init-auto-save)
;;; init-auto-save.el ends here
