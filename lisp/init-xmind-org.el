;;; init-xmind-org.el --- Completion with xmind-org -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:




(use-package xmind-org
  :ensure t
  :init)





(provide 'init-xmind-org)
;;; init-xmind-org.el ends here
