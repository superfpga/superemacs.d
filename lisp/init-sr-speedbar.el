;;; init-sr-speedbar.el --- Completion with sr-speedbar -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package sr-speedbar
  :ensure t
  :config
  (setq sr-speedbar-right-side nil)
  (setq sr-speedbar-width 30)
  (setq dframe-update-speed t)
  )




(provide 'init-sr-speedbar)
;;; init-sr-speedbar.el ends here
