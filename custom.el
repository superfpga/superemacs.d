(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(sanityinc-tomorrow-bright))
 '(package-selected-packages
   '(envrc uptimes shfmt dotenv-mode htmlize gnuplot sudo-edit eat lua-mode flycheck-ledger ledger-mode origami regex-tool info-colors flycheck-clojure cider elein cljsbuild-mode clojure-ts-mode cask-mode flycheck-relint cl-libify package-lint-flymake highlight-quoted macrostep aggressive-indent immortal-scratch auto-compile ipretty elisp-slime-nav puni paredit justl just-mode nginx-mode nixpkgs-fmt nix-ts-mode terraform-mode docker-compose-mode dockerfile-mode docker yaml-mode flycheck-rust rust-mode flycheck-nim nim-mode j-mode dune-format dune tuareg sqlformat projectile-rails yard-mode bundler yari robe ruby-compilation inf-ruby rspec-mode ruby-hash-syntax add-node-modules-path psci purescript-mode elm-test-runner elm-mode dhall-mode haskell-mode reformatter toml-mode ruff-format flymake-ruff pip-requirements restclient httprepl haml-mode css-eldoc sass-mode rainbow-mode tagedit org-pomodoro writeroom-mode org-cliplink smarty-mode php-mode js-comint xref-js2 prettier-js typescript-mode js2-mode json-mode erlang csv-mode markdown-mode textile-mode crontab-mode alert ibuffer-projectile projectile flymake-actionlint github-review forge github-clone bug-reference-github yagist git-commit magit-todos magit git-link git-timemachine git-modes vc-darcs whitespace-cleanup-mode which-key highlight-escape-sequences whole-line-or-region move-dup page-break-lines multiple-cursors avy browse-kill-ring symbol-overlay rainbow-delimiters mode-line-bell vlf list-unicode-display unfill mmm-mode session windswap switch-window corfu-terminal corfu orderless marginalia embark-consult consult embark vertico consult-eglot eglot flymake-flycheck flymake ibuffer-vc rg wgrep anzu diff-hl diredfl disable-mouse default-text-scale dimmer color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized command-log-mode scratch diminish esup gcmh exec-path-from-shell gnu-elpa-keyring-update seq clojure-mode nix-mode org-modern org-roam-ui org-roam general expand-region evil-visualstar evil-surround evil-nerd-commenter evil-matchit evil-escape evil))
 '(session-use-package t nil (session)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
