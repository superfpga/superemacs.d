;;; init-evil-nerd-commenter.el --- Completion with evil-nerd-commenter -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package evil-nerd-commenter
  :ensure t
  :init )






(provide 'init-evil-nerd-commenter)
;;; init-evil-nerd-commenter.el ends here
