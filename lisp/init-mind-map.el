;;; init-org-mind-map.el --- Completion with org-mind-map -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



;; (require-package 'org-mind-map)
;; (org-mind-map-mode t)

(use-package org-mind-map
  :ensure t
  :init (org-mind-map-mode))





(provide 'init-org-mind-map)
;;; init-org-mind-map.el ends here
