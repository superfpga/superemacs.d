;;; init-org-transclusion.el --- Completion with org-transclusion -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package org-transclusion
  :ensure t
  :after org
  :init
  )




(provide 'init-org-transclusion)
;;; init-org-transclusion.el ends here
