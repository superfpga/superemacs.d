;;; init-org-noter.el --- Completion with org-noter -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:




(use-package org-noter
  :ensure t)




(provide 'init-org-noter)
;;; init-org-noter.el ends here
