;;; init-evil-visualstar.el --- Completion with evil-visualstar -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:





(use-package evil-visualstar
  :ensure t
  :init (global-evil-visualstar-mode))








(provide 'init-evil-visualstar)
;;; init-evil-visualstar.el ends here
