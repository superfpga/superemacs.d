 #!/bin/sh
set -e
# Gets the path where the current shell is located
SHELL_FOLDER=$(cd "$(dirname "$0")";pwd)

echo "The folder path is being changed to：$SHELL_FOLDER"

cd $SHELL_FOLDER

read -p  "Please fill in the information submitted by commit:" msg

# Null value judgment
if [ ! $msg ]; then
  echo "Terminate the commit because the commit description is empty."
else
  echo "Start the add-commit operation......"
  # git add -A && git commit -m "$msg"
  git add -u && git commit -m "$msg"

  echo "After commit, start pulling and pushing the code."
  git pull && git push

  # Determine whether the previous command was successful
  if [ $? -eq 0 ]; then
    echo "The process ends and the submission is complete."
   else
    echo "Something went wrong, please resolve the error."
   fi
fi
