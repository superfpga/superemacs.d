;;; init-evil-surround.el --- Completion with evil-surround -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:




(use-package evil-surround
  :ensure t
  :init (global-evil-surround-mode))







(provide 'init-evil-surround)
;;; init-evil-surround.el ends here
