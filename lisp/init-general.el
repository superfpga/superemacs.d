;;; init-general.el --- Completion with general -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package general
  ;; :straight t
  ;; :after evil
  :ensure t
  :config
  (setq general-emit-autoloads nil)

  (general-define-key
   :states '(normal insert motion emacs)
   :keymaps 'override
   :prefix-map 'tyrant-map
   :prefix "SPC"
   :non-normal-prefix "M-SPC")

  (general-create-definer tyrant-def :keymaps 'tyrant-map)
  (tyrant-def "" nil)

  (general-create-definer despot-def
    :states '(normal insert motion emacs)
    :keymaps 'override
    :major-modes t
    :prefix "SPC m"
    :non-normal-prefix "M-SPC m")
  (despot-def "" nil)

  (general-def universal-argument-map
    "SPC u" 'universal-argument-more)

  (tyrant-def
    "SPC"     '("M-x" . execute-extended-command)
    "TAB"     '("last buffer" . alternate-buffer)
    "!"       '("shell cmd" . shell-command)

    "a"       (cons "applications" (make-sparse-keymap))
    "ac"      'calc-dispatch
    "ap"      'list-processes
    "aP"      'proced
    "ar"      'align-regexp

    "b"       (cons "buffers" (make-sparse-keymap))
    "bb"      'switch-to-buffer
    "bB"      'ibuffer
    "bd"      'kill-current-buffer
    "bm"      'switch-to-messages-buffer
    "bs"      'switch-to-scratch-buffer
    "bu"      'reopen-killed-buffer
    "bx"      'kill-buffer-and-window

    "c"       (cons "code" (make-sparse-keymap))
    "cb"      'flymake-show-buffer-diagnostics
    "cc"      'compile
    "cn"      'next-error
    "cp"      'previous-error
    "cr"      'recompile
    "cx"      'kill-compilation
    "c="      'indent-region-or-buffer
    "cl"      'comment-line
    "cy"      'evilnc-copy-and-comment-lines
    "ct"      'customize-themes
    "co"      'customize-option-other-window

    "e"       (cons "mesure" (make-sparse-keymap))
    "ee"      'sanityinc/eval-last-sexp-or-region
    "es"      'esup

    "f"       (cons "files" (make-sparse-keymap))
    "fC"      '("copy-file" . write-file)
    "fD"      'delete-current-buffer-file
    "fe"      'find-library
    "fE"      'sudo-edit
    "ff"      'find-file
    "fj"      'dired-jump
    "fJ"      'dired-jump-other-window
    "fR"      'rename-current-buffer-file
    "fs"      'save-buffer
    "fv"      (cons "variables" (make-sparse-keymap))
    "fvd"     'add-dir-local-variable
    "fvf"     'add-file-local-variable
    "fvp"     'add-file-local-variable-prop-line
    "fc"      'open-bashrc-file
    "fi"      'open-init-file
    "fl"      'open-leader-file
    "fo"      'open-local-file

    "F"       (cons "frame" (make-sparse-keymap))
    "Fd"      'delete-frame
    "FD"      'delete-other-frames
    "Fn"      'make-frame
    "Fo"      'other-frame

    "h"       (cons "help" (make-sparse-keymap))
    "ha"      'apropos
    "hb"      'describe-bindings
    "hc"      'describe-char
    "hf"      'describe-function
    "hF"      'describe-face
    "hi"      'info-emacs-manual
    "hI"      'info-display-manual
    "hk"      'describe-key
    "hK"      'describe-keymap
    "hm"      'describe-mode
    "hM"      'woman
    "hp"      'describe-package
    "ht"      'describe-text-properties
    "hv"      'describe-variable
    "hP"      (cons "profiler" (make-sparse-keymap))
    "hPs"     'profiler-start
    "hPk"     'profiler-stop
    "hPr"     'profiler-report

    "j"       (cons "jump" (make-sparse-keymap))
    "ji"      'imenu
    "jg"      'avy-goto-char-timer

    "k"       (cons "kill" (make-sparse-keymap))
    "kk"      'kill-buffer
    "ke"      'kill-emacs

    "l"       (cons "layouts" tab-prefix-map)
    "ld"      'tab-bar-close-tab
    "lD"      'tab-bar-close-other-tabs
    "lg"      'tab-bar-change-tab-group
    "lm"      'tab-bar-move-tab-to
    "lM"      'tab-bar-move-tab-to-group
    "ll"      'tab-bar-switch-to-tab
    "lR"      'tab-bar-rename-tab
    "lt"      'other-tab-prefix
    "lu"      'tab-bar-undo-close-tab
    "l1"      '("select tab 1..8" . tab-bar-select-tab)
    "l2"      'tab-bar-select-tab
    "l3"      'tab-bar-select-tab
    "l4"      'tab-bar-select-tab
    "l5"      'tab-bar-select-tab
    "l6"      'tab-bar-select-tab
    "l7"      'tab-bar-select-tab
    "l8"      'tab-bar-select-tab
    "l TAB"   'tab-bar-switch-to-last-tab

    "m"       (cons "major mode" (make-sparse-keymap))

    "o"       (cons "roam" (make-sparse-keymap))
    "of"      'org-roam-node-find
    "oi"      'org-roam-node-insert
    "ou"      'org-roam-ui-open

    "p"       (cons "projects" project-prefix-map)
    "pt"      'project-open-in-tab
    "pi"      'package-install
    "pd"      'package-delete
    "pl"      'list-packages
    "pu"      'package-upgrade-all

    "q"       (cons "quit" (make-sparse-keymap))
    "qd"      'restart-emacs-debug-init
    "qr"      'restart-emacs
    "qR"      'restart-emacs-without-desktop
    "qf"      'delete-frame
    "qq"      'save-buffers-kill-terminal
    "qQ"      'save-buffers-kill-emacs

    "r"       (cons "restart" (make-sparse-keymap))
    "rr"      'restart-emacs
    "rs"      'replace-string

    "s"       (cons "spelling" (make-sparse-keymap))
    "sb"      'flyspell-buffer
    "sn"      'flyspell-goto-next-error
    "sr"      'flyspell-region
    "ss"      'save-buffer
    "sf"      'switch-to-buffer
    "sw"      'switch-window

    "T"       (cons "toggles" (make-sparse-keymap))
    "Ta"      'auto-fill-mode
    "Td"      'toggle-debug-on-error
    "Tf"      'display-fill-column-indicator-mode
    "Tl"      'toggle-truncate-lines
    "Tm"      'flymake-mode
    "Tn"      'display-line-numbers-mode
    "Ts"      'flyspell-mode
    "Tw"      'whitespace-mode
    "TW"      'toggle-word-wrap

    "u"       '("universal arg" . universal-argument)

    "v"       (cons "verilog" (make-sparse-keymap))
    "vi"      (cons "indent" (make-sparse-keymap))
    "vib"     'verilog-indent-buffer

    "w"       (cons "windows" (make-sparse-keymap))
    "w TAB"   'alternate-window
    "w+"      'window-layout-toggle
    "wb"      'switch-to-minibuffer-window
    "wc"      'whitespace-cleanup
    "wd"      'kill-buffer
    "wD"      'delete-other-windows
    "wm"      'toggle-maximize-buffer
    "wf"      'follow-mode
    "wh"      'evil-window-left
    "wH"      'evil-window-move-far-left
    "wj"      'evil-window-down
    "wJ"      'evil-window-move-very-bottom
    "wk"      'evil-window-up
    "wK"      'evil-window-move-very-top
    "wl"      'evil-window-right
    "wL"      'evil-window-move-far-right
    "wr"      'rotate-windows-forward
    "wR"      'rotate-windows-backward
    "ws"      'split-window-vertically
    "wS"      'split-window-vertically-and-focus
    "wt"      'toggle-current-window-dedication
    "wu"      'winner-undo
    "wU"      'winner-redo
    "wv"      'split-window-horizontally
    "wV"      'split-window-horizontally-and-focus)
  )


(provide 'init-general)
;;; init-general.el ends here
