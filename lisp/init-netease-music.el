;;; init-netease-music.el --- Completion with netease-music -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(add-to-list 'load-path "~/.emacs.d/site-lisp/netease-cloud-music")


;; Require it
(require 'netease-cloud-music)
(require 'netease-cloud-music-ui)       ;If you want to use the default TUI, you should add this line in your configuration.
(require 'netease-cloud-music-comment)  ;If you want comment feature



(provide 'init-netease-music)
;;; init-netease-music.el ends here
