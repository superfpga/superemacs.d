;;; init-org-gtd.el --- Completion with org-gtd -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:

(setq org-gtd-update-ack "3.0.0")


(use-package org-gtd
  :ensure t
  :init (org-gtd-mode))





(provide 'init-org-gtd)
;;; init-org-gtd.el ends here
