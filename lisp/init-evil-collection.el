;;; init-evil-collection.el --- Completion with evil-collection -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:


(require-package 'evil-want-keybinding)

(setq evil-want-integration t) ;; This is optional since it's already set to t by default.
(setq evil-want-keybinding nil)
(require 'evil)
(when (require-package 'evil-collection)
  (evil-collection-init))



(provide 'init-evil-collection)
;;; init-evil-collection.el ends here
