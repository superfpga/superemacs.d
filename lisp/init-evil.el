;;; init-evil.el --- Completion with evil -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:




(use-package evil
  :ensure t
  :init (evil-mode))





(provide 'init-evil)
;;; init-evil.el ends here
