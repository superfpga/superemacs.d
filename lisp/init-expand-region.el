;;; init-expand-region.el --- Completion with expand-region -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:





(use-package expand-region
  :ensure t
  :init)





(provide 'init-expand-region)
;;; init-expand-region.el ends here
