;;; init-evil-escape.el --- Completion with evil-escape -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:





(use-package evil-escape
  :ensure t
  :config
  (evil-escape-mode)
  )






(provide 'init-evil-escape)
;;; init-evil-escape.el ends here
