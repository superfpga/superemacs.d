;;; init-verilog-ext.el --- Completion with verilog-ext -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:





(use-package verilog-ext
  :ensure t
  :after verilog-mode
  :demand
  :hook ((verilog-mode . verilog-ext-mode))
  :init
  ;; Can also be set through `M-x RET customize-group RET verilog-ext':
  ;;  - Verilog Ext Feature List (provides info of different features)
  ;; Comment out/remove the ones you do not need
  (setq verilog-ext-feature-list
        '(font-lock
          xref
          capf
          hierarchy
          eglot
          lsp
          flycheck
          beautify
          navigation
          template
          formatter
          compilation
          imenu
          which-func
          hideshow
          typedefs
          time-stamp
          block-end-comments
          company-keywords
          ports))
  :config
  (verilog-ext-mode-setup))







(provide 'init-verilog-ext)
;;; init-verilog-ext.el ends here
