;;; init-keyfreq.el --- Completion with keyfreq -*- lexical-binding: t -*-
;;; Commentary:
;;; Code:



(use-package keyfreq
  :ensure t
  :config
  (keyfreq-mode)
  (keyfreq-autosave-mode)
  (setq keyfreq-excluded-commands
        '(self-insert-command
          forward-char
          backward-char
          previous-line
          next-line))
  )





(provide 'init-keyfreq)
;;; init-keyfreq.el ends here
